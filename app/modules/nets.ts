import { createLogger, format, transports } from "winston";
import * as request from "request";
import _ = require("lodash");
import sha256 = require("sha256");
// import { CustomLogger } from "./logger";

/**
 * @author Radek Kowal
 *
 */
export class NETSIntegration {

    private _urlOrder = "";
    private _secret: any;
    private _apiKey: any;
    private _mode = "";
    private _signature: any;
    private _data: string = "";
    private _enc: string = "utf8";
    // private logger: any = new CustomLogger().create();

    public async initialize(requestBody: any) {
        this._mode = requestBody.mode;


        this._data = requestBody.data;

        this._enc = requestBody.enc;

        if (this._mode == "UAT") {
            // https://uat-api.nets.com.sg:9065/uat/merchantservices/qr/dynamic/v1/order/request
            this._urlOrder = 'https://uat-api.nets.com.sg:9065/uat/merchantservices/qr/dynamic/v1/order/request';
        } else if (this._mode == "Production") {
            this._urlOrder = 'https://api.nets.com.sg/merchantservices/qr/dynamic/v1/order/request';
        }

        this._secret = requestBody.secret; // "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
        this._apiKey = requestBody.apiKey; // "8bc63cde-2647-4a78-ac75-d5f534b56047";
    }

    private logger = createLogger({
        format: format.combine(
            format.splat(),
            format.simple()
        ),
        transports: [
            new transports.Console({ level: "debug" }),
            new transports.File({ filename: "nets.log", level: "debug" })
        ]
    });


    public sendQRRequest() {
        this.logger.info(JSON.stringify(this._data));

        let input = JSON.stringify(this._data) + this._secret;
        this.logger.debug(input);

        // let sh = sha256(input);
        // this.logger.debug(sh);

        // var m1 = sh.match(/\w{2}/g);
        // this.logger.debug(m1);

        // var afterMap = m1.map(function (a) {
        //     return String.fromCharCode(parseInt(a, 16));
        // }).join('');

        // this.logger.debug(afterMap);

        // let buf = new Buffer(afterMap, this._enc);
        // let base64Encoded = buf.toString("base64");
        // let base64Encoded = Buffer.from(afterMap).toString("base64");

        let base64Encoded = require("crypto")
            .createHash("sha256")
            .update(input, "ascii")
            .digest("base64");

        this.logger.log("info", "Buffer encoding set to: %s", this._enc);
        this.logger.log("info", "Signature encoded to base64: %s", base64Encoded);

        this._signature = base64Encoded;

        let options = {
            headers: {
                "KeyId": this._apiKey,
                "Sign": this._signature,
                "Content-Type": "application/json"
            },
            url: this._urlOrder,
            json: true,
            body: this._data
        };

        return new Promise((resolve: (value: any) => void, reject: (error: any) => void) => {
            request.post(options, (error, response) => {
                if (error) {
                    this.logger.error(error);
                    reject(error);
                }
                if (response !== undefined) {
                    this.logger.debug(response.body);
                    if (response.statusCode != 200) {
                        // if (status && status.startsWith("E")) {
                        reject(_.get(response, "body.message"));
                        // }
                    } else {
                        this.logger.info(JSON.stringify(response.body));
                        let qrCode = response.body.txn_identifier;

                        // $('#payment_image').html('<img id="netspay-mobile-link" class="hidden-xs" height="180" width="180" src="data:image/png;base64,' + json_result.qr_code + '" /> <a href="netspay://payment/?source=com.nets.netspay&amp;urlscheme=netspay&amp;qrdata=' + encodeURIComponent(qrCode) + '"> <img class="hidden-md hidden-lg height="180" width="180" " src="./assets/images/netspay.png" /></a>');
                        resolve(qrCode);
                    }
                } else {
                    reject("No response on POST QR Code Request!");
                }
            });
        });
    }
}