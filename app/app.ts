const express = require("express");
const bodyParser = require('body-parser');
import { NETSIntegration } from "./modules/nets";
import { createLogger, format, transports } from "winston";

const logger = createLogger({
    format: format.combine(
        format.timestamp(),
        format.simple()
    ),
    transports: [
        new transports.Console({ level: "debug" }),
        new transports.File({ filename: "nets.log", level: "debug" })
    ]
});

let app = express();
let router = express.Router();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies
app.use('/', router);

app.listen(3003, function () {
    console.log('Example app listening on port 3003!');
});

router.get('/', function (req: any, res: any) {
    res.send('Hello World!');
});

router.post("/nets", async (req: any, res: any) => {
    try {
        logger.warn("Processing NETS started");
        const netsIntegration = new NETSIntegration();
        await netsIntegration.initialize(req.body);
        let result = await netsIntegration.sendQRRequest();
        res.send(result);
    } catch (e) {
        logger.error(e);
        res.send(e);
    }
});
